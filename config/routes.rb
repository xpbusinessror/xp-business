require 'api_constraints'
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  	
  	# API Routes
  	devise_for :users, :skip => [:sessions, :passwords, :registrations]

	namespace :api, defaults: {format: :json} do
	    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
	    	namespace :user_app do
			    devise_for :users, :controllers => {
			        :sessions => "api/v1/user_app/sessions",
			        :passwords => "api/v1/user_app/passwords"
			    }

			    resource :user, only: [:show, :update]

			    resource :registrations do 
			    	member do
			    		post :send_otp
			    		get :validate_email
			    	end
			    end

			    resource :businesses do 
			    	collection do
			    		get :categories
			    	end 
			    	member do 
			    		post :upload_images
			    		patch :service_times
			    	end
			    end
	    	end
		end
    end


    #For admin
    namespace :admin do
	    scope module: :v1 do

	    end
    end  
end
