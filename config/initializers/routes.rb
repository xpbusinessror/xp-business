module ActionDispatch
  module Journey
    class Routes
      def simulator
        @simulator ||= begin
          gtg = GTG::Builder.new(ast).transition_table unless ast.blank?
          GTG::Simulator.new(gtg)
        end
      end
    end
  end
end