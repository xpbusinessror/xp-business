class ExpireOtpJob < ApplicationJob
  queue_as :default

  def perform(user, type1)
    # Do something later
    user.update(otp: nil) if type1.eql?('registration')
    user.update(access_token: nil) if type1.eql?('forgot_password')
  end
end
