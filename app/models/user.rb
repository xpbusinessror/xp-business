class User < ApplicationRecord

  include DefaultEnum::RegistrationRole
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable

  has_many :password_histories, dependent: :destroy
  has_many :businesses, dependent: :destroy 

  accepts_nested_attributes_for :businesses, reject_if: proc { |attributes| attributes[:name].blank? }
 
  validates :password, unique_password: true
  validates_presence_of :device_type, :device_token

	def generate_jwt
    JsonWebToken.encode(user_id: self.id)
  end

  private

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["lower(email) = :value", {:value => login.downcase}]).first
    else
      where(conditions.to_hash).first
    end
  end

  def self.create_otp
    6.times.map{rand(10)}.join
  end
end
