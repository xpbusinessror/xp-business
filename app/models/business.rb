class Business < ApplicationRecord
	extend Geocoder::Model::ActiveRecord
	geocoded_by :address,
    	latitude: :fetched_latitude,
    	longitude: :fetched_longitude
  	reverse_geocoded_by :latitude, :longitude

	#associations
	belongs_to :user
	has_many :service_times, :dependent => :destroy
	has_many :bus_images, :as => :galleryable, :dependent => :destroy

	accepts_nested_attributes_for :service_times, :allow_destroy => true
	accepts_nested_attributes_for :bus_images, reject_if: proc { |attributes| attributes[:media].blank? }

	#enumerations
	enum time_schedule: [:fixed, :flexible]
	enum status: [:pending, :approved, :rejected, :suspended]

	#validations
	after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }
	# validates_presence_of :designation, :address, :business_phone_no, :owned_by, :established_on

	#scopes
	scope :approved,->{ where(status: 1) }
end