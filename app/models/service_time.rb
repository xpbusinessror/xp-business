class ServiceTime < ApplicationRecord
  belongs_to :business

  enum day: [:Sun, :Mon, :Tue, :Wed, :Thu, :Fri, :Sat]
  enum service_type: [:open_close, :delivery, :room_service]
end
