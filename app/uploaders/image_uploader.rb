class ImageUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  include CarrierWave::MiniMagick
  include ::CarrierWave::Video
  include ::CarrierWave::Video::Thumbnailer

  # Choose what kind of storage to use for this uploader:
  storage :aws

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :video_thumb_large, if: :is_video? do
    process thumbnail: [
      { format: 'png', size: 400, seek: '1%', logger: Rails.logger }
    ]

    def full_filename(for_file)
      png_name for_file, version_name
    end
    process :apply_png_content_type
  end

  version :video_thumb_200, if: :is_video? do
    process thumbnail: [
      { format: 'png', quality: 10, size: 200,seek: '1%', logger: Rails.logger }
    ]

    def full_filename(for_file)
      png_name for_file, version_name
    end

    process :apply_png_content_type
  end

  version :video_thumb_300, if: :is_video? do
    process thumbnail: [
      { format: 'png', quality: 10, size: 300, seek: '1%', logger: Rails.logger }
    ]

    def full_filename(for_file)
      png_name for_file, version_name
    end

    process :apply_png_content_type
  end

  version :image_thumb_large, if: :is_image? do
    # process resize_to_fill: [100,100]
    process :resize_to_fit => [400, 400]
  end

  version :image_thumb_200, if: :is_image? do
    process :resize_to_fit => [200, 200]
  end

  version :image_thumb_300, if: :is_image? do
    process :resize_to_fit => [300, 300]
  end

  def png_name for_file, version_name
    data12 = %Q{#{version_name}_#{for_file.chomp(File.extname(for_file))}.png}
  end

  def apply_png_content_type(*)
    file.instance_variable_set(:@content_type, 'image/png')
  end

  protected
  def is_image?(picture)
    ['jpg', 'jpeg', 'gif', 'png','svg'].include?(picture.extension.to_s.downcase)
  end
  def is_video?(picture)
    ['mp4', 'avi', 'mpg', 'm3u8', 'm3u', 'mpeg', 'mpeg', 'mov', 'mpeg-4'].include?(picture.extension.to_s.downcase)
  end
end
