class Api::V1::UserApp::BusinessesController < Api::V1::ApplicationController
	before_action :find_business, only: [:upload_images, :update, :service_times, :destroy]

	def categories
		@categories = BusinessCategory.all
	end

	def create
		@business = @user.businesses.new(business_params)
		reponse_message 'Something went worng!', 500 unless @business.save!
	end

	def update
		save_images_or_service_times
	end

	def upload_images
		save_images_or_service_times
	end

	def service_times
		save_images_or_service_times
	end

	private

	def business_params
		params.require(:business).permit(:id, :name, :latitude, :longitude, :address, :description, :country_code, :owned_by, :established_on, :business_phone_no, :user_id, :designation, :business_category_id, service_times_attributes: [:id, :day, :from, :to, :is_open, :service_type, :business_id, :_destroy], bus_images_attributes: [:id, :media, :media_flag, :resolution, :height, :width, :_destroy])
	end

	def find_business
		@business = Business.find_by(id: params[:business][:id])
	end

	def save_images_or_service_times
		@business.update(business_params)
		reponse_message 'Something went worng!', 500 unless @business.save!
	end
end