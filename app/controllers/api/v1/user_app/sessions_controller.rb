# class Api::V1::UserApp::SessionsController < Devise::OmniauthCallbacksController
class Api::V1::UserApp::SessionsController < Api::V1::ApplicationController
  skip_before_action :authenticate_request, only: [:create]
	protect_from_forgery with: :null_session
	include ExceptionHandler

  	def create
	    resource = User.find_for_database_authentication(login: params[:session][:email])
    	return invalid_login_attempt unless resource
	    if resource.valid_password?(params[:session][:password])
	      sign_in(:user, resource)
	      current_user.update(session_params) if current_user
	    end
  	end

  	def destroy
      sign_out(@user)
      @user.update_attributes(device_token: nil, device_type: nil)
  	end

    private

    def session_params
    	params.require(:session).permit(:device_token, :device_type)
    end
end