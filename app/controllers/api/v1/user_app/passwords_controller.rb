class Api::V1::UserApp::PasswordsController < Api::V1::ApplicationController
	skip_before_action :authenticate_request
	
	def new
		user =  User.find_by(email: request.env["HTTP_EMAIL"])
		if user.present?
			user.update(access_token: User.create_otp)
			UserMailer.send_otp_for_forgot_password(user).deliver_now
			Thread.new{
				ExpireOtpJob.new(user, 'forgot_password').enqueue(wait: 3.minutes)
			}
		else
			response_message 'Email not found.', 500
		end
	end

	def update
		user = User.find_by(email: params[:password][:email])
		if user.access_token.eql?(params[:password][:otp])
			status = user.update!(password_params)
			user.password_histories.create(password: params[:password][:password])
		else
			response_message 'Either Otp expired Or Invalid otp.', 500
		end
	end

	private

	def password_params
		params.require(:password).permit(:password)
	end
end