class Api::V1::UserApp::RegistrationsController < Api::V1::ApplicationController
	skip_before_action :authenticate_request
	
	#To validate email: email is present in database or not?
  	def validate_email
	    email1 = request.env["HTTP_EMAIL"].downcase.split('@')
	    email = email1[0].gsub(/[.]/, '')+'@'+email1[1] 
	    user1 = User.find_by(email: email)
	    user2 = User.find_by(email: request.env["HTTP_EMAIL"])

	    if (user1.present? && user1.email_verified_flag.eql?(1))
	      response_message 'Email already registered.', 406
	    elsif (user2.present? && user2.email_verified_flag.eql?(1))
	      response_message 'Email already registered.', 406
	    elsif TempUser.find_by(email: request.env["HTTP_EMAIL"]).present?
	      response_message "All's good.", 200 
	    else
	      user1.destroy if user1.present?
	      user2.destroy if user2.present?
	      response_message "All's good.", 200
	    end
  	end

  	#send otp before registration
	def send_otp
		temp_user = TempUser.find_or_create_by(email: params[:registration][:email].downcase)
		if temp_user.present?
			pre_registration_process(temp_user)
		else
			response_message 'Not a valid email.', 401
		end
	end

	#register new user
	def create
		temp_user = TempUser.find_by(email: params[:registration][:email].downcase)
		if temp_user.present?
			if temp_user.otp.eql?(params[:registration][:otp])
				user = User.create!(registration_params)
				user.password_histories.create!(password: params[:registration][:password])
				sign_in(:user, user)
				temp_user.destroy
			else
				response_message 'Either Otp expired Or Invalid otp.', 500
			end
		else
			response_message 'Email Not found', 204
		end
	end

	private

	def pre_registration_process(temp_user)
		otp = User.create_otp
		temp_user.update(otp: otp)
		Thread.new{
			UserMailer.send_otp_for_registration(temp_user).deliver_now
			ExpireOtpJob.new(temp_user, 'registration').enqueue(wait: 3.minutes)
		}
	end

	def registration_params
		params[:registration][:role] = "business"
		params[:registration][:email_verified_flag] = 1
		params.require(:registration).permit(:email, :password, :email_verified_flag, :role, :device_token, :device_type, :businesses_attributes=>[:id, :name])
	end
end