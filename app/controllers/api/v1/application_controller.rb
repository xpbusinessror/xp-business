class Api::V1::ApplicationController < ApplicationController
	include ExceptionHandler
	protect_from_forgery with: :exception, unless: -> { request.format.json? }
	protect_from_forgery prepend: true

	before_action :authenticate_request
  private

  def authenticate_request
    @user ||= User.find(decoded_auth_token[:user_id]) if decoded_auth_token
    response_message 'Not Authorized', 401 unless (@user || nil)
  end

  private

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  def http_auth_header
    if request.env["HTTP_AUTHORIZATION"].present?
      return request.env["HTTP_AUTHORIZATION"].split(' ').last
    else 
      # response_message 'Missing Token', 401
    end
    nil
  end
end