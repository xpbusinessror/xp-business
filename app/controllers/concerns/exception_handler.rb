module ExceptionHandler
	extend ActiveSupport::Concern

	# Define custom error subclasses - rescue catches `StandardErrors`
	class AuthenticationError < StandardError; end
	class MissingToken < StandardError; end
	class InvalidToken < StandardError; end
	class ExpiredSignature < StandardError; end
	class DecodeError < StandardError; end

  	included do
	    # Define custom handlers
	    rescue_from Exception, :with => :error_generic
	    rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two
	    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
	    rescue_from ExceptionHandler::MissingToken, with: :four_twenty_two
	    rescue_from ExceptionHandler::InvalidToken, with: :four_twenty_two
	    rescue_from ExceptionHandler::ExpiredSignature, with: :four_ninety_eight
	    rescue_from ExceptionHandler::DecodeError, with: :four_zero_one

	    rescue_from ActiveRecord::RecordNotFound do |e|
	     render json: { message: e.message }, status: :not_found
	    end

	    rescue_from ActiveRecord::RecordInvalid do |e|
	      render json: { message: e.message }, status: :unprocessable_entity
	    end
	end

	def invalid_login_attempt
    	render json: {
        	message: "Invalid email or password", 
        	code: 401 
        }, status: :unauthorized and return
    end

	def error_generic(e)
	    case "#{e.class}"
	      when "ActiveRecord::StatementInvalid"
	        msg = 'you have to pass all mandatory parameters.'
	      when "ActionView::Template::Error"
	      	no_content
	      else
	        not_defined_error e
	      end
    end

    def not_defined_error error
    	render json: {
        	message: "#{error}", 
        	code: 500 
        }, status: :not_defined_error and return
    end

	def no_content
		render json: {
        	message: "No template found.", 
        	code: 204 
        }, status: :no_content and return
	end
	
	#response message to all pages for api
    def response_message message, code
        render json: {message: message, code: code} and return
    end

    #pagination for apis
    def pages_message current_page ,all_pages
        {current_page: current_page,page_size: ENV['SIZE'],total_pages: all_pages}
    end

    private

	# JSON response with message; Status code 422 - unprocessable entity
	def four_twenty_two(e)
	   render json: { message: e.message, code: 422 }, status: :unprocessable_entity
	end
	 
	# JSON response with message; Status code 401 - Unauthorized
	def four_ninety_eight(e)
	    render json: { message: e.message, code: 401 }, status: :invalid_token
	end

	  # JSON response with message; Status code 401 - Unauthorized
	def four_zero_one(e)
	    render json: { message: e.message, code: 401 }, status: :invalid_token
	end

	   # JSON response with message; Status code 401 - Unauthorized
	def unauthorized_request(e)
	    render json: { message: e.message, code: 401 }, status: :unauthorized
	end
end