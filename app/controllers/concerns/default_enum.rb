module DefaultEnum
	module RegistrationRole
	    extend ActiveSupport::Concern
	    
		included do
	  		enum role: [:normal, :business]
	  		enum device_type: [:web, :ios, :android]
		end
	end
end