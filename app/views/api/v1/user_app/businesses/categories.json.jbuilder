json.categories @categories do |category|
  json.(category, :id, :name, :image_url, :thumbnail_url)
end
# json.token current_user.generate_jwt
json.code 200
json.message 'Categories list'