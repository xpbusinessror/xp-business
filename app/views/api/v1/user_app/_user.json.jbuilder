json.(user, :id, :email)
json.token user.generate_jwt
json.businesses user.businesses, :id, :name
