class UserMailer < ApplicationMailer
	default from: "XP <support@thexpapp.com>"
	
	def send_otp_for_registration(temp_user)
		@user = temp_user
	    @subject = "Confirm your email address"
	    mail(
	      to: @user.email,
	      subject: @subject
	    )
	end

	def send_otp_for_forgot_password(user)
		@user = user
	    @subject = "Password Reset Instructions"
	    mail(
	      to: @user.email,
	      subject: @subject
	    )
	end
end
