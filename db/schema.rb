# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190129075738) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_notis", force: :cascade do |t|
    t.string "title", default: ""
    t.text "message", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "app_infos", force: :cascade do |t|
    t.float "version_number"
    t.float "build_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bank_details", force: :cascade do |t|
    t.string "account_holder_name", null: false
    t.string "bank_name", null: false
    t.string "account_number", null: false
    t.string "bank_routing_number", null: false
    t.boolean "set_as_default", default: false
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "block_event_post_users", force: :cascade do |t|
    t.integer "event_id"
    t.integer "post_id"
    t.integer "user_id"
    t.integer "host_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blocks", force: :cascade do |t|
    t.integer "user_id"
    t.integer "block_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "business_categories", force: :cascade do |t|
    t.string "name"
    t.text "image_url"
    t.text "thumbnail_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "business_working_days", force: :cascade do |t|
    t.string "day"
    t.integer "business_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "businesses", force: :cascade do |t|
    t.string "cover_image_url"
    t.string "thumbnail_url"
    t.string "name"
    t.text "description"
    t.text "address"
    t.string "start_time"
    t.string "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "designation"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
    t.integer "status", default: 0
    t.integer "user_id"
    t.integer "business_category_id"
    t.integer "video_flag"
    t.string "business_phone_no", default: ""
    t.string "alternate_business_phone_no", default: ""
    t.float "avg_ratings", default: 0.0
    t.integer "is_featured", default: 0
    t.string "thumb_300_image", default: ""
    t.integer "is_watch_intro_video"
    t.date "established_on"
    t.string "owned_by"
    t.string "country_code"
    t.integer "time_schedule"
  end

  create_table "cards", force: :cascade do |t|
    t.string "fingerprint"
    t.integer "card_number"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cause_invitations", force: :cascade do |t|
    t.integer "cause_id"
    t.integer "invitation_from"
    t.integer "invitation_to"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "causes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "host_id"
    t.integer "event_category_id"
    t.string "title"
    t.string "sub_title"
    t.string "description"
    t.string "cause_longitute"
    t.string "cause_latitute"
    t.string "things_to_donate"
    t.string "cause_place_name"
    t.string "cause_place_address"
    t.string "image"
    t.string "image_name"
    t.integer "video_flag"
    t.string "donation_longitute"
    t.string "donation_latitute"
    t.string "donation_place_address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "channel_categories", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "channel_records", force: :cascade do |t|
    t.integer "channel_id"
    t.text "story"
    t.text "detail"
    t.string "image"
    t.string "video"
    t.integer "event_memory_flag", limit: 2, default: 0
    t.integer "post_flag", limit: 2, default: 0
    t.integer "live_event_flag", limit: 2, default: 0
    t.integer "event_id"
    t.integer "view"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "channels", force: :cascade do |t|
    t.string "channel_name"
    t.integer "user_id"
    t.integer "subscribed_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.string "image_link"
    t.string "cover_image_link"
    t.string "cover_image_name"
    t.string "description"
    t.integer "public_flag", default: 0
    t.string "created_by"
    t.string "admin_image"
  end

  create_table "chat_reads", force: :cascade do |t|
    t.integer "chatroom_id"
    t.integer "message_id"
    t.integer "user_id"
    t.integer "is_seen", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chat_statuses", force: :cascade do |t|
    t.integer "is_online", default: 0
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chatroom_id"
  end

  create_table "chatroom_activities", force: :cascade do |t|
    t.integer "user_id"
    t.integer "chatroom_id"
    t.text "activity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chatroom_notifications", force: :cascade do |t|
    t.integer "is_mute", default: 0
    t.integer "chatroom_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chatroom_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "chatroom_id"
    t.integer "is_admin", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "is_online", default: 0
    t.string "color", default: ""
  end

  create_table "chatrooms", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "image"
    t.integer "is_group", default: 0
    t.string "group_desc", default: ""
    t.string "creator_name", default: ""
    t.string "thumbnail_image", default: ""
  end

  create_table "chats", force: :cascade do |t|
    t.string "message"
    t.integer "sender_id"
    t.integer "receiver_id"
    t.integer "group_flag", default: 0
    t.integer "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "claims", force: :cascade do |t|
    t.string "address", default: ""
    t.string "phone_no", default: ""
    t.string "email", default: ""
    t.boolean "is_approved", default: false
    t.string "business_name", default: ""
    t.string "designation_of_location", default: ""
    t.integer "event_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer "user_id"
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "video_flag", default: 0
    t.integer "commentable_id", null: false
    t.string "commentable_type", null: false
  end

  create_table "contents", force: :cascade do |t|
    t.integer "content_type", null: false
    t.string "title", default: ""
    t.text "content", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conversations", force: :cascade do |t|
    t.integer "sender_id"
    t.integer "receiver_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chatroom_id"
    t.index ["receiver_id"], name: "index_conversations_on_receiver_id"
    t.index ["sender_id"], name: "index_conversations_on_sender_id"
  end

  create_table "cover_videos", force: :cascade do |t|
    t.string "image"
    t.integer "video_flag"
    t.string "thumb_300"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
    t.string "thumbnail"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "current_events", force: :cascade do |t|
    t.string "event_video"
    t.string "event_image"
    t.string "title"
    t.string "sub_title"
    t.string "event_place_name"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
    t.date "date_of_event"
    t.text "description"
    t.string "things_to_bring"
    t.string "things_people_get"
    t.integer "event_category_id"
    t.string "host_name"
    t.integer "user_id"
    t.integer "availability"
    t.datetime "start_event_date"
    t.datetime "end_event_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views", default: 0
    t.integer "group_event_flag", default: 0
    t.integer "group_event_id"
    t.string "event_place_address"
    t.string "video_flag"
    t.string "event_image_name"
    t.integer "admin_block", default: 0
    t.string "cause_address"
    t.string "cause_donation_type"
    t.string "cause_latitude"
    t.string "cause_longitude"
    t.string "thumbnail_image"
    t.float "duration"
    t.boolean "is_claim_location", default: false
    t.text "keywords", default: ""
    t.string "admin_image"
    t.string "thumb_300_image"
    t.integer "is_featured", default: 0
    t.boolean "is_suspend", default: false
    t.integer "is_publish", default: 0
    t.integer "is_mad_event", default: 0
    t.boolean "is_rewarded", default: false
    t.integer "created_by", default: 0
    t.integer "donation_type"
    t.float "money", default: 0.0
    t.text "clothes"
    t.integer "is_watch_intro_video"
    t.boolean "is_deleted", default: false
    t.datetime "expiration_date"
    t.integer "edit_count", default: 0
  end

  create_table "delete_messages", force: :cascade do |t|
    t.integer "user_id"
    t.integer "message_ids", default: [], array: true
    t.string "chatroom_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "donations", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_id"
    t.integer "donation_type"
    t.boolean "show_user", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "charge_id"
    t.integer "paid_user_id"
    t.float "amount", default: 0.0
    t.integer "payment_status"
    t.text "payment_response"
    t.string "refund_id"
    t.string "transaction_id", default: ""
  end

  create_table "event_blocks", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_categories", force: :cascade do |t|
    t.string "event_name", default: ""
    t.string "color_code", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.string "admin_image"
  end

  create_table "event_post_likes", force: :cascade do |t|
    t.integer "event_id"
    t.integer "user_id"
    t.integer "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "timeline_flag"
    t.integer "channel_id"
  end

  create_table "expire_events", force: :cascade do |t|
    t.string "event_video"
    t.string "event_image"
    t.string "title"
    t.string "sub_title"
    t.string "event_place_name"
    t.string "latitute"
    t.string "longitude"
    t.date "date_of_event"
    t.text "description"
    t.string "things_to_bring"
    t.string "things_people_get"
    t.integer "event_category_id"
    t.string "host_name"
    t.integer "user_id"
    t.integer "availability"
    t.string "start_event_date"
    t.string "end_event_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views", default: 0
    t.string "event_place_address"
    t.string "video_flag"
    t.string "event_image_name"
    t.integer "admin_block", default: 0
    t.integer "event_id"
  end

  create_table "favourites", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_id"
    t.integer "expire_event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.integer "type_id"
  end

  create_table "filter_histories", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active"
    t.integer "business_category_id"
  end

  create_table "flyers", force: :cascade do |t|
    t.string "flyer_image"
    t.string "flyer_title"
    t.integer "event_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "follow_and_recommendations", force: :cascade do |t|
    t.integer "user_id"
    t.integer "business_id"
    t.string "type"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "follows", force: :cascade do |t|
    t.integer "following_id", null: false
    t.integer "follower_id", null: false
    t.boolean "blocked", default: false
    t.integer "follow_status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["follower_id"], name: "index_follows_on_follower_id"
    t.index ["following_id", "follower_id"], name: "index_follows_on_following_id_and_follower_id", unique: true
    t.index ["following_id"], name: "index_follows_on_following_id"
  end

  create_table "friends", force: :cascade do |t|
    t.integer "user_id"
    t.integer "friend_id"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "confermed_request", default: 0
    t.integer "block_via"
  end

  create_table "galleries", force: :cascade do |t|
    t.string "media"
    t.integer "media_flag"
    t.string "resolution"
    t.string "height"
    t.string "width"
    t.string "galleryable_type"
    t.integer "galleryable_id"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "group_blocks", force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "group_members", force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "in_group", default: 1
    t.integer "host_flag", default: 0
  end

  create_table "groups", force: :cascade do |t|
    t.string "group_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "creator_name"
    t.string "image_name"
    t.string "image_link"
    t.string "description"
    t.integer "user_id"
    t.integer "video_flag", default: 0
    t.string "latitute"
    t.string "longitute"
    t.integer "public_flag", default: 0
  end

  create_table "hash_tags", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hosts", force: :cascade do |t|
    t.integer "event_id"
    t.integer "user_id"
    t.integer "expire_event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.integer "join_flag", default: 0
    t.integer "public_event_flag", default: 0
  end

  create_table "images", force: :cascade do |t|
    t.string "bucket_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "intro_videos", force: :cascade do |t|
    t.string "video_url", default: ""
    t.string "thumbnail", default: ""
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invitation_notis", force: :cascade do |t|
    t.integer "invitation_type"
    t.boolean "is_read", default: false
    t.text "noti_msg", default: ""
    t.integer "user_id"
    t.integer "noti_via_user_id"
    t.integer "event_id"
    t.boolean "is_accept", default: false
    t.string "noti_title", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category", default: ""
    t.integer "post_id"
  end

  create_table "likes", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "likeable_id", null: false
    t.string "likeable_type", null: false
    t.integer "status", default: 0
  end

  create_table "logs", force: :cascade do |t|
    t.integer "loggable_id", null: false
    t.string "loggable_type", null: false
    t.text "content", default: ""
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "points", default: 0
    t.string "further_type", default: ""
    t.integer "further_type_id"
    t.boolean "is_deleted", default: false
  end

  create_table "message_reads", force: :cascade do |t|
    t.integer "last_msg_index"
    t.integer "user_id"
    t.integer "chatroom_id"
    t.integer "is_seen"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chatroom_id"
    t.integer "user_id"
    t.integer "content_type"
    t.integer "is_seen", default: 0
    t.string "thumbnail_image", default: ""
    t.string "uniq_msg_id"
    t.string "thumb_50_image", default: ""
  end

  create_table "notifications", force: :cascade do |t|
    t.text "usernames"
    t.string "notification_title"
    t.string "notification_content"
    t.string "action"
    t.string "category"
    t.integer "status", limit: 2, default: 0
    t.integer "system_notify", limit: 2, default: 0
    t.integer "user_id"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "notification_message"
    t.integer "event_id"
    t.integer "notification_via"
    t.integer "group_id", default: 0
    t.integer "post_id"
    t.integer "channel_id"
    t.integer "business_id"
    t.integer "review_id"
    t.string "notifiable_type"
    t.integer "notifiable_id"
    t.integer "comment_id"
  end

  create_table "password_histories", force: :cascade do |t|
    t.integer "user_id"
    t.string "encrypted_password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_id"
    t.integer "donation_id"
    t.decimal "amount"
    t.string "transaction_id"
    t.datetime "transaction_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_blocks", force: :cascade do |t|
    t.integer "post_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_events", force: :cascade do |t|
    t.integer "user_id"
    t.integer "event_id"
    t.integer "video_flag", default: 0
    t.string "thumbnail"
    t.string "image_url"
    t.text "content"
    t.string "resolution"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_name"
    t.string "image_height"
    t.string "image_width"
    t.integer "checkin_flag", default: 0
    t.integer "channel_flag", default: 0
    t.float "duration", default: 0.0
    t.integer "channel_id"
    t.boolean "is_active", default: false
    t.integer "channel_category_id"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
    t.string "posted_by"
    t.string "admin_image"
    t.integer "business_id"
    t.string "thumb_300_image"
    t.string "address", default: ""
    t.string "hash_tag", default: [], array: true
    t.integer "is_intro_video_post", default: 0
  end

  create_table "post_views", force: :cascade do |t|
    t.integer "user_id"
    t.integer "post_event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "push_noti_records", force: :cascade do |t|
    t.integer "event_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rails_push_notifications_apns_apps", force: :cascade do |t|
    t.text "apns_dev_cert"
    t.text "apns_prod_cert"
    t.boolean "sandbox_mode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rails_push_notifications_gcm_apps", force: :cascade do |t|
    t.string "gcm_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rails_push_notifications_mpns_apps", force: :cascade do |t|
    t.text "cert"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rails_push_notifications_notifications", force: :cascade do |t|
    t.text "destinations"
    t.integer "app_id"
    t.string "app_type"
    t.text "data"
    t.text "results"
    t.integer "success"
    t.integer "failed"
    t.boolean "sent", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["app_id", "app_type", "sent"], name: "app_and_sent_index_on_rails_push_notifications"
  end

  create_table "report_abuses", force: :cascade do |t|
    t.integer "user_id"
    t.integer "group_id"
    t.integer "event_id"
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "spam_user_id"
    t.integer "post_id"
  end

  create_table "reports", force: :cascade do |t|
    t.integer "reportable_id"
    t.string "reportable_type"
    t.integer "user_id"
    t.text "content", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "reviewable_id"
    t.string "reviewable_type"
    t.integer "rating", default: 0
    t.float "avg_rating", default: 0.0
    t.text "content", default: ""
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "search_records", force: :cascade do |t|
    t.integer "user_id"
    t.float "latitude"
    t.float "longitude"
    t.text "keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seens", force: :cascade do |t|
    t.integer "user_id"
    t.integer "story_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_times", force: :cascade do |t|
    t.integer "day"
    t.time "from"
    t.time "to"
    t.boolean "is_open"
    t.integer "service_type"
    t.bigint "business_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_service_times_on_business_id"
  end

  create_table "stories", force: :cascade do |t|
    t.string "image_url", default: ""
    t.integer "story_type"
    t.string "thumbnail", default: ""
    t.string "image_height", default: ""
    t.string "image_width", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "duration", default: 0.0
    t.integer "storyable_id"
    t.string "storyable_type"
    t.integer "user_id"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
    t.string "thumb_300_image", default: ""
    t.string "orientation", default: ""
    t.index ["storyable_id", "storyable_type"], name: "index_stories_on_storyable_id_and_storyable_type"
  end

  create_table "sub_categories", force: :cascade do |t|
    t.string "name", default: ""
    t.string "image", default: ""
    t.integer "event_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subcription_records", force: :cascade do |t|
    t.integer "channel_id"
    t.integer "user_id"
    t.integer "status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.integer "event_category_id"
    t.integer "sub_category_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "distance", default: 0
  end

  create_table "tags", force: :cascade do |t|
    t.integer "post_event_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.integer "comment_id"
  end

  create_table "temp_users", force: :cascade do |t|
    t.string "email"
    t.string "otp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_details", force: :cascade do |t|
    t.integer "business_category_id"
    t.integer "event_category_id"
    t.integer "user_id"
    t.integer "active_screen"
    t.integer "distance", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "is_private", default: 0
    t.integer "points", default: 0
    t.integer "survey_filter"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "fullname"
    t.string "image"
    t.text "description"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "admin_flag", default: 0
    t.boolean "active_account", default: true
    t.string "status"
    t.string "gender"
    t.string "country"
    t.string "state"
    t.string "city"
    t.integer "email_verified_flag"
    t.string "token_authenticatable"
    t.string "authentication_token"
    t.datetime "authentication_token_created_at"
    t.integer "forget_password_flag"
    t.string "access_token"
    t.string "forget_password_code"
    t.string "dob"
    t.string "facebook_uid"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
    t.string "device_token"
    t.string "thumbnail"
    t.datetime "email_verify_date"
    t.integer "allow_notification", default: 1
    t.string "image_name"
    t.integer "age"
    t.string "occupation", default: ""
    t.text "about_me", default: ""
    t.string "interest", default: ""
    t.string "cover_image_name", default: ""
    t.string "cover_image_link", default: ""
    t.integer "device_type"
    t.string "phone_no"
    t.integer "role"
    t.string "company_name"
    t.string "country_code"
    t.string "referral_code"
    t.string "customer_id"
    t.integer "is_business_status", default: 0
    t.integer "is_online", default: 0
    t.string "address", default: ""
    t.string "username", default: ""
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "validate_email_phones", force: :cascade do |t|
    t.string "email_or_phone"
    t.string "pin"
    t.boolean "is_verified"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "views", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "viewable_id"
    t.string "viewable_type"
  end

  create_table "withdrawls", force: :cascade do |t|
    t.integer "event_id"
    t.float "amount", default: 0.0
    t.integer "status", default: 0
    t.text "other_info", default: ""
    t.integer "user_id"
    t.integer "bank_detail_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "transaction_id"
  end

  add_foreign_key "service_times", "businesses"
end
