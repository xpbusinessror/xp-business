class CreateGalleries < ActiveRecord::Migration[5.1]
  def change
    create_table :galleries do |t|
      t.string :media
      t.integer :media_flag
      t.string :resolution
      t.string :height
      t.string :width
      t.string :galleryable_type
      t.integer :galleryable_id
      t.string :type

      t.timestamps
    end
  end
end
