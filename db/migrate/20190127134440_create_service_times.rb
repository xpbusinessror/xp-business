class CreateServiceTimes < ActiveRecord::Migration[5.1]
  def change
    create_table :service_times do |t|
      t.integer :day
      t.time :from
      t.time :to
      t.boolean :is_open
      t.integer :service_type
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
