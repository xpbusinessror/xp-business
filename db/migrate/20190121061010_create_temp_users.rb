class CreateTempUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :temp_users do |t|
      t.string :email
      t.string :otp

      t.timestamps
    end
  end
end
