class AddEstablishedOnOwnedByCountryCodeToBusiness < ActiveRecord::Migration[5.1]
  def change
    add_column :businesses, :established_on, :date
    add_column :businesses, :owned_by, :string
    add_column :businesses, :country_code, :string
    add_column :businesses, :time_schedule, :integer
  end
end
